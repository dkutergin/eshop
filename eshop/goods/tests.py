from django.test import TestCase

from .models import Category, Good, Order

class Tests(TestCase):

	def test_category(self):
		c = Category.objects.create(name='CATEGORY')
		id = 0
		if c != None:
			id = c.id
			c.delete()
		self.assertEqual(id > 0, True)
	
	def test_good(self):
		c = Category.objects.create(name='CATEGORY')
		id = 0
		if c != None:
			g = Good.objects.create(category=c, name='GOOD', price=1, count=1)
			if g != None:
				id = g.id
				g.delete()
			c.delete()
		self.assertEqual(id > 0, True)

	def test_order(self):
		c = Category.objects.create(name='CATEGORY')
		id = 0
		if c != None:
			g = Good.objects.create(category=c, name='GOOD', price=1, count=2)
			if g != None:
				o = g.create_order(count=1)
				if o != None:
					id = o.id
					o.delete()
				g.delete()
			c.delete()

		self.assertEqual(id > 0, True)
		