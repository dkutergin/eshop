# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-04 08:12
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('goods', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 4, 8, 12, 10, 57000, tzinfo=utc), verbose_name='Creation Date'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='take_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 4, 8, 12, 20, 516000, tzinfo=utc), verbose_name='Take Date'),
            preserve_default=False,
        ),
    ]
