from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from .models import Category, Good, Order

def index(request):
	categories = Category.objects.order_by('-name')
	goods = Good.objects.order_by('-name')
	context = {'categories': categories, 'goods': goods}
	return render(request, 'catalog.html', context)

def good_info(request, good_id):
	good = get_object_or_404(Good, pk=good_id)
	rest = good.rest()
	return render(request, 'good.html', {'good': good, 'rest': rest})

def order_create(request):
	good_id = request.POST['good_id']
	count = request.POST['count']
	good = get_object_or_404(Good, pk=good_id)
	order = good.create_order(count)
	if order != None:
		return HttpResponseRedirect(reverse('goods:order_info', args=(order.id,)))
	else:
		return render(request, 'error.html',
		{'message': 'Cannot create order for "{1}" (x {0})'.format(count, good.name)})

def order_info(request, order_id):
	order = get_object_or_404(Question, pk=order_id)
	status = "Ready" if order.status == 0 else "Not ready"
	cost = order.good.cost(order.count)
	context = {'order': order, 'status': status, 'cost': cost}
	return render(request, 'order.html', context)

def hello(request):
	return render(request, 'error.html', {'message': 'Hello World!'})

def show_error(request, message):
	return render(request, 'error.html', {'message': message})