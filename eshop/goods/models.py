from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

# Category of goods
class Category(models.Model):
	name = models.CharField(max_length=100)

# Good
class Good(models.Model):
	category = models.ForeignKey(Category)
	name = models.CharField(max_length=100)
	price = models.IntegerField(default=0)
	count = models.IntegerField(default=0)
	# get rest goods count
	def rest(self):
		result = self.count
		os = Order.objects.filter(good=self, status=0)
		for o in os:
			result -= o.count
		return result
	# get total cost of goods
	def cost(self, count):
		return self.price * count
	# create order
	def create_order(self, count):
		if self.rest() >= count:
			date = timezone.now()
			order = Order(good=self, count=count, creation_date=date)
			order.save()
			return order
		else:
			return None

# Order
class Order(models.Model):
	good = models.ForeignKey(Good)
	count = models.IntegerField(default=0)
	address = models.CharField(max_length=200)
	phone = models.CharField(max_length=20)
	creation_date = models.DateTimeField(default=timezone.now())
	take_date = models.DateTimeField(default=timezone.now())
	status = models.IntegerField(default=0)
	# take order
	def take(self):
		if self.status == 0:
			if self.good.count > self.count:
				self.good.count -= self.count
				self.status = 1
				self.take_date = timezone.now()
