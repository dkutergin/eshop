from django.conf.urls import url

from . import views

urlpatterns = [
	# /
	url(r'^$', views.index, name="index"),
	# /ID/
	url(r'^(?P<good_id>[0-9]+)/$', views.good_info, name="good_info"),
	# /order/ID/
	url(r'^order/(?P<order_id>[0-9]+)/$', views.order_info, name="order_info"),
	# /order/create/
	url(r'^order/create/$', views.order_create, name="order_create"),
	# /hello/
	url(r'^hello/$', views.hello, name="hello"),
	# /error/MESSAGE/
	url(r'^error/(?P<message>[A-Za-z0-9\s_,.:;!-"]+)/$', views.show_error, name="show_error"),
]