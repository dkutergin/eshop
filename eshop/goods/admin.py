from django.contrib import admin

from .models import Category, Good, Order

admin.site.register(Category)
admin.site.register(Good)
admin.site.register(Order)
